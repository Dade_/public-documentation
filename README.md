# Public documentation

[Technical structure](https://gitlab.com/Dade_/public-documentation/-/wikis/Technical-Structure)

[Local environment setup](https://gitlab.com/Dade_/public-documentation/-/wikis/Local-Setup)(contains detailed instructions to start developing literally in one click)


AWS Infrastructure, it's written in terraform (private)


[Deployment procedure](https://gitlab.com/Dade_/public-documentation/-/wikis/Deployment)
